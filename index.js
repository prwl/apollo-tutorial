import express from 'express';
import http from 'http';
import { ApolloServer, gql } from 'apollo-server-express';

import pubsub, { EVENTS } from './subscription';

const typeDefs = gql`
    type Query {
        hello: String
    }
    type Mutation {
      createMessage(text: String!): String!
    } 
    type Subscription {
      messageCreated: String!
    }
`;

const resolvers = {
    Query: {
        hello: () => 'Hello World!'
    },
    Mutation: {
        createMessage: (parent, args) => {
          const message = args.text;
	pubsub.publish(EVENTS.MESSAGE, {
          messageCreated: message,
        });
          return message;
      },
    },
    Subscription: {
     messageCreated: {
       subscribe: () => pubsub.asyncIterator(EVENTS.MESSAGE),
     },
   },
};

const server = new ApolloServer({ typeDefs, resolvers });
const app = express();
server.applyMiddleware({ app });

const httpServer = http.createServer(app);
server.installSubscriptionHandlers(httpServer);

httpServer.listen({ port: 4000 }, () =>
  console.log(`🚀 Server ready at http://localhost:4000${server.graphqlPath}`)
);
