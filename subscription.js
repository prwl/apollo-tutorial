import { PubSub } from 'apollo-server';

export const CREATED = 'CREATED';

export const EVENTS = {
  MESSAGE: CREATED,
};

export default new PubSub();
